<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Officers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officers', function (Blueprint $table) {
            $table->id();
            $table->string('uid', 100);
            $table->string('lname', 100);
            $table->string('fname', 100);
            $table->string('mi', 100);
            $table->string('email', 100);
            $table->string('phone', 100);
            $table->string('position', 100);
            $table->string('bio', 500);
            $table->string('profile', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officers');
    }
}
