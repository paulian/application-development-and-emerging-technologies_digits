<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $uid = "admin-607add0a06445";
        $lname = md5("admin");
        $fname = "Administrator";
        $mi = "A";
        $email = md5("admin@admin.com");
        $phone = "00000000000";
        $position = "Administrator";
        $bio = "";
        $profile = "default.png";

        DB::table('officers')->insert([
            'uid' => $uid,
            'lname' => $lname,
            'fname' => $fname,
            'mi' => $mi,
            'email' => $email,
            'phone' => $phone,
            'position' => $position,
            'bio' => $bio,
            'profile' => $profile
        ]);
    }
}
