<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OfficerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $index = 8;
        $i = 5;
        $uid = array(8);
        while($index >= 0){
            $uid[$index] = uniqid();
            $lname = ["Dumdum","Manadong","Cloma","Abueg","Corales","Berongoy","Grefiel","Gad","Baldesco"];
            $fname = ["Paul Ian","Florelyn","Cherlie Kate","Abbie","Blane Cyndelle","Ma. Desiree","Riza","Apple May","Ray Jade"];
            $mi = ["A","E","C","P","C","S","T","A","C"];
            $position = ["President","Vice-President","Secretary","Treasurer","Auditor","P.I.O","P.I.O","Sgt@Arms","Sgt@Arms"];
            DB::table('officers')->insert([
                'uid' => $uid[$index],
                'lname' => $lname[$index],
                'fname' => $fname[$index],
                'mi' => $mi[$index],
                'email' => strtolower($lname[$index])."@gmail.com",
                'phone' => "0912345678",
                'position' => $position[$index],
                'bio' => "Proud Member of DIGITS Organization.",
                'profile' => "default.png",
                'created_at' => "2021-01-10 ".$i.":00:00",
                'updated_at' => "2021-01-10 ".$i.":00:00"
            ]);
            $i++;
            $index--;
        }
        $index = 8;
        $i = 5;
        while($index >= 0){
            DB::table('messages')->insert([
                'chatid' => $uid[$index],
                'senderid' => "admin-607add0a06445",
                'message' => "Hello ".$fname[$index].", thank you for being a part of our organization. We hope for your active participation towards a great success for our organization. Welcome and enjoy friend, have a great day.",
                'type' => "message",
                'file' => "null",
                'read' => "unread",
                'created_at' => "2021-01-10 ".$i.":00:00",
                'updated_at' => "2021-01-10 ".$i.":00:00"
            ]);
            $i++;
            $index--;
        }
    }
}
