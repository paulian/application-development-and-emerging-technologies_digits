<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/login-css.php";?>   
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/login-navbar.php";?>
        <div class="display-container">
                <div class="modal fade" aria-hidden="true" id="register">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('register.store') }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h5><span class="fa fa-edit"></span> Create Officer Account</h5>
                                </div>
                                <div class="modal-body modal-body-custom">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Last Name</label>
                                                <input required type="text" class="form-control" name="lname">
                                            </div>
                                            <div class="col-md-5">
                                                <label>First Name</label>
                                                <input required type="text" class="form-control" name="fname">
                                            </div>
                                            <div class="col-md-2">
                                                <label>MI</label>
                                                <input maxlength="1" required type="text" class="form-control" name="mi">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input required type="email" class="form-control" name="email">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label>Phone No.</label>
                                                <input required type="number" class="form-control" name="phone">
                                            </div>
                                            <div class="col-md-7">
                                                <label>Position</label>
                                                <input required type="text" class="form-control" name="position">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                    <button class="btn btn-success btn-sm"><span class="fa fa-save"></span> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <div class="l-container">
            </div>
            <div class="r-container">
                <form action="{{ route('login.store') }}" method="POST">
                @csrf
                @method('POST')
                <div class="modal-header">
                    <h6><span class="fa fa-user"></span> Sign In</h6>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="btn-group col-md-12" style="margin: 0px; padding: 0px;">
                            <button disabled type="button" class="btn btn-custom fa fa-user"></button>
                            <input required type="text" placeholder="Username" class="form-control" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btn-group col-md-12" style="margin: 0px; padding: 0px;">
                            <button disabled type="button" class="btn btn-custom fa fa-lock"></button>
                            <input required type="password" placeholder="Password" class="form-control" name="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="mr-5">
                        <h6>Don't have and account? <a href="#" data-toggle="modal" data-target="#register">Register</a></h6>
                    </div>
                    <button class="btn btn-secondary btn-sm"><span class="fa fa-sign-in"></span> Sign In</button>
                </div>
                </form>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "password-warning"){
            ?>
                toastr.warning("Invalid username or password.");
            <?php
                }
                if(session('toastType') == "success-register"){
            ?>
                toastr.success("Registered successfuly.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script>
    </body>
</html>