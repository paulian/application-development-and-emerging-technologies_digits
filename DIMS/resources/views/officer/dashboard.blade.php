<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/officer-css.php";?>   
        <?php if(session('user-theme') == 'dark'){ include "addons/includes/user-dark-css.php"; }?>
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/officer-navbar.php";?>
        <div class="display-container">
            <div class="modal-header" style="border-left: solid white 1px">
                <h6><span class='fa fa-tachometer'></span> Dashboard</h6>
                <!--<div>
                    <button class="btn btn-sm"><span class="fa fa-plus"></span> Create New Class</button>
                </div>-->
            </div>

            <div class="display">
                <div class="row" style="padding: 15px; margin: 0px;">
                    <div class="col-md-4">
                        <div>
                            <h3>{{ $o }}</h3>
                            <h5><span class='fa fa-users'></span> Officers</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <h3>{{ $e }}</h3>
                            <h5><span class='fa fa-flag'></span> Events</h5>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <h3>{{ $m }}</h3>
                            <h5><span class='fa fa-check'></span> Meetings</h5>
                        </div>
                    </div>
                </div>
    
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-bell'></span> Annoucements</h6>
                </div>
                <br>
                <table class="table-hover">
                    <tbody>
                        @foreach ($announcements as $announcement)
                        <tr style="display: flex; justify-content: space-between; border-bottom: solid 1px gray;">
                            <td class="ml-3" style="width: 850px;">
                                <h6><span class="fa fa-user"></span> {{ $announcement->announcer }} : {{ date('M d, Y - h:i A ', strtotime($announcement->created_at)) }}</h6>
                                <div class="ml-4">{{ $announcement->details }}</div>
                            </td>
                            <td>
                                <button data-toggle="modal" data-target="#announcementDetails{{ $announcement->id }}" class="btn btn-success btn-sm"><span class="fa fa-info"></span> Details</button>
                            </td>
                        </tr>

                        <div class="modal fade" aria-hidden="true" id="announcementDetails{{ $announcement->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                     <div class="modal-header">
                                        <h5><span class="fa fa-bell"></span> Announcement Details</h5>
                                    </div>
                                    <div class="modal-body" style="padding: 20px;">
                                        <div class="form-group">
                                            <label>Announcer</label>
                                            <input readonly value="{{ $announcement->announcer }}" required type="text" class="form-control" name="announcer">
                                        </div>
                                        <div class="form-group">
                                            <label>Announcement</label>
                                            <textarea readonly rows="10" required class="form-control" name="details">{{ $announcement->details }}</textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button data-dismiss="modal" class="btn btn-success btn-sm"><span class="fa fa-check"></span> OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <?php include "addons/navigations/officer-footer.php";?>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>
    </body>
</html>