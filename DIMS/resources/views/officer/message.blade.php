<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/officer-css.php";?>
        <?php if(session('user-theme') == 'dark'){ include "addons/includes/user-dark-css.php"; }?>   
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/officer-navbar.php";?>
        <div class="display-container">
            <div class="modal-header" style="border-left: solid white 1px">
                <h6><span class='fa fa-envelope'></span> Messages</h6>
            </div>
            <div class="display">
                <div class="modal-body">
                    <h6><span class='fa fa-user'></span> You are currently logged in as <b>{{ $user->fname }} {{ $user->mi }}. {{ $user->lname }}</b></h6>
                    <div class="message-board" id="scrolling_to_bottom">
                        @foreach($messages as $message)
                            <?php 
                                if($message->senderid == "admin-607add0a06445"){
                                    if($message->type != "message"){
                                        if($message->type == "fa fa-file-photo-o"){
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>Administrator:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <a href="../../addons/assets/root/mfiles/'.$message->file.'"><img class="message-img" src="../../addons/assets/root/mfiles/'.$message->file.'" alt=""></a>
                                                    </div>
                                                </div>
                                            ';
                                        }else if($message->type == "fa fa-file-video-o"){
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>Administrator:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <video class="message-img" controls="" name="media"><source src="../../addons/assets/root/mfiles/'.$message->file.'" type="audio/mpeg"></video>
                                                    </div>
                                                </div>
                                            ';
                                        }else{
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>Administrator:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <a class="download-link" href="../../addons/file_download_support/downloadFile.php?file='.$message->file.'&name='.$message->message.'">
                                                            <div class="btn-group col-md-7">
                                                                <input disabled value="'.$message->message.'" class="form-control">
                                                                <button class="btn btn-dark '.$message->type.'" ></button>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                    }
                                    else{
                                        echo '
                                            <div class="l-con">
                                                <label><span class="fa fa-user"></span> <b>Administrator:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                <div class="form-group">
                                                    <button class="btn btn-dark">'.$message->message.'</button>
                                                </div>
                                            </div>
                                        ';
                                    }
                                }
                                elseif($message->senderid == session('user-uid')){
                                    if($message->type != "message"){
                                        if($message->type == "fa fa-file-photo-o"){
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <a href="../../addons/assets/root/mfiles/'.$message->file.'"><img class="message-img" src="../../addons/assets/root/mfiles/'.$message->file.'" alt=""></a>
                                                    </div>
                                                </div>
                                            ';
                                        }else if($message->type == "fa fa-file-video-o"){
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <video class="message-img" controls="" name="media"><source src="../../addons/assets/root/mfiles/'.$message->file.'" type="audio/mpeg"></video>
                                                    </div>
                                                </div>
                                            ';
                                        }else{
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                    <div class="form-group">
                                                        <a class="download-link" href="../../addons/file_download_support/downloadFile.php?file='.$message->file.'&name='.$message->message.'">
                                                            <div class="btn-group col-md-7">
                                                                <button class="btn btn-success '.$message->type.'" ></button>
                                                                <input disabled value="'.$message->message.'" class="form-control">
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                    }
                                    else{
                                        echo '
                                            <div class="r-con">
                                                <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($message->created_at)).'</i></label>
                                                <div class="form-group">
                                                    <button class="btn btn-success">'.$message->message.'</button>
                                                </div>
                                            </div>
                                        ';
                                    }
                                }
                            ?>
                        @endforeach
                    </div>
                    <form action="{{ route('send-message.store') }}" method="POST">               
                        @csrf
                        @method('POST')
                        <div class="send-board">
                            <button data-toggle="modal" data-target="#browseFile" type="button" class="btn btn-success"><span class="fa fa-plus"></span></button>
                            <input autofocus="true" placeholder="Aa" required name="message" type="text" class="form-control">
                            <button type="submit" class="btn btn-success"><span class="fa fa-send"></span></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <h6>Note: Only the administrator of the system can recieve and view your message.</h6>
            </div>
            <?php include "addons/navigations/officer-footer.php";?>
            <div class="modal fade" id="browseFile" area-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <form action="../addons/file_upload_support/uploadfile.php?sen={{ session('user-uid') }}&rec={{ session('user-uid') }}&command=user" method="POST" enctype="multipart/form-data">
                        <div class="modal-body" style="padding: 10px;">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="new_file" id="upload" onchange="displayname(this,$(this))">
                                        <label class="custom-file-label" for="upload">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save</button>
                            <button data-dismiss="modal" class="btn btn-dark btn-sm"><span class="fa fa-times"></span> Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>   
        <script>
            $('#scrolling_to_bottom').animate({
                scrollTop: $('#scrolling_to_bottom').get(0).scrollHeight}, 0);
        </script>
    </body>
</html>