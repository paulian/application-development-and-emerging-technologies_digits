<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>
        <?php include "addons/includes/master-js.php";?>
        <?php include "addons/includes/officer-css.php";?>  
        <?php if(session('user-theme') == 'dark'){ include "addons/includes/user-dark-css.php"; }?> 
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/officer-navbar.php";?>
        <div class="display-container">
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-group'></span> Officers</h6>
                </div>
                <div style="padding: 15px;">
                    <h6><span class='fa fa-user'></span> You are currently logged in as <b>{{ $user->fname }} {{ $user->mi }}. {{ $user->lname }}</b></h6>
                    <hr style="border-bottom: solid 1px gray;">
                </div>
                <table class="table-hover">
                    <thead>
                        <tr>
                            <td>Profile</td>
                            <td>Name</td>
                            <td>Email Address</td>
                            <td>Phone Number</td>
                            <td>Position</td>
                            <td style="width: 180px">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($officers as $officer)
                        <?php if($officer->position != "Administrator"){?>
                            <tr>
                            <td class="pl-3 table-profile"><img src="../../addons/assets/root/profiles/{{ $officer->profile }}"></td>
                                <td class="pl-3">{{ $officer->lname }}, {{ $officer->fname }} {{ $officer->mi }}.</td>
                                <td>{{ $officer->email }}</td>
                                <td>{{ $officer->phone }}</td>
                                <td>{{ $officer->position }}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#viewProfile{{ $officer->id }}" class="btn btn-success btn-sm"><span class="fa fa-user"></span> Profile</button>
                                </td>
                            </tr>
                        <?php }?>

                        <div class="modal fade" aria-hidden="true" id="viewProfile{{ $officer->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-user"></span> Profile</h5>
                                        </div>
                                        <div class="modal-body" style="padding: 20px;">
                                            <div class="form-group">
                                                <div class="officer-profile">
                                                    <img src="../../addons/assets/root/profiles/{{ $officer->profile }}" alt="">
                                                </div>
                                                <hr class="profile-bar">
                                                    <textarea disabled class="form-control bio" rows="3">{{ $officer->bio }}</textarea>
                                                <hr class="profile-bar">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label>Last Name</label>
                                                        <input type="hidden" value="{{ $officer->id }}" class="form-control" name="id">
                                                        <input readonly type="text" value="{{ $officer->lname }}" class="form-control" name="lname">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label>First Name</label>
                                                        <input readonly type="text" value="{{ $officer->fname }}" class="form-control" name="fname">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>MI</label>
                                                        <input readonly type="text" value="{{ $officer->mi }}" class="form-control" name="mi">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input readonly type="text" value="{{ $officer->email }}" class="form-control" name="email">
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label>Phone No.</label>
                                                        <input readonly type="text" value="{{ $officer->phone }}" class="form-control" name="phone">
                                                    </div>
                                                    <div class="col-md-7">
                                                        <label>Position</label>
                                                        <input readonly type="text" value="{{ $officer->position }}" class="form-control" name="position">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-success btn-sm"><span class="fa fa-check"></span> OK</button>
                                        </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            <?php include "addons/navigations/officer-footer.php";?>
        </div>
        <?php include "addons/includes/master-js.php";?>
    </body>
</html>