<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/officer-css.php";?> 
        <?php if(session('user-theme') == 'dark'){ include "addons/includes/user-dark-css.php"; }?>  
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/officer-navbar.php";?>
        <div class="display-container">
            <form action="{{ route('user-account.update', $officer->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-user'></span> Account Management</h6>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2" style="height: 200px; text-align: center; padding: 20px;" class="profile-container">
                            <img src="../../addons/assets/root/profiles/{{ $officer->profile }}" class="profile">
                            <a style="margin-top: 20px;" class="btn btn-primary" data-toggle="modal" data-target="#browseProfile" href="#"><span class="fa fa-edit"></span> Change Profile</a>
                        </div>
                        <div class="col-md-10">
                            <label>Bio</label>
                            <br>
                            <textarea style="padding: 10px;" name="bio" cols="130" rows="9"><?php echo $officer->bio;?></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="hidden" value="{{ $officer->id }}" class="form-control" name="id">
                                <input required value="{{ $officer->lname }}" type="text" class="form-control" name="lname">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input required value="{{ $officer->fname }}" type="text" class="form-control" name="fname">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Middle Innitial</label>
                                <input required value="{{ $officer->mi }}" type="text" class="form-control" name="mi">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input required value="{{ $officer->email }}" type="email" class="form-control" placeholder="***@gmail.com" name='email'>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input required value="{{ $officer->phone }}" type="number" class="form-control" placeholder="+63 *** *** ****" name="phone">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Officer Position</label>
                                <input required value="{{ $officer->position }}" type="text" class="form-control" name="position">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save Changes</button>
                    </div>
                </div>
                <?php include "addons/navigations/officer-footer.php";?>
            </form>
        </div>
        <div class="modal fade" id="browseProfile" area-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <form action="../addons/file_upload_support/uploadProfile.php?id={{ $officer->id }}&currentProfile={{ $officer->profile }}" method="POST" enctype="multipart/form-data">
                        <div class="modal-body" style="padding: 10px;">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="new_profile" id="upload" onchange="displayname(this,$(this))">
                                        <label class="custom-file-label" for="upload">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-sm"><span class="fa fa-save"></span> Save</button>
                            <button data-dismiss="modal" class="btn btn-dark btn-sm"><span class="fa fa-times"></span> Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>   
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "account-updated"){
            ?>
                toastr.info("Changes has been saved.");
            <?php
                }
                if(session('toastType') == "profile-updated"){
            ?>
                toastr.info("Your profile has been updated.");
            <?php
                }
                if(session('toastType') == "profile-invalid"){
            ?>
                toastr.warning("You uploaded an invalid file.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script>  
    </body>
</html>