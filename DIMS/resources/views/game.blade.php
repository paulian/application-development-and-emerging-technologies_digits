<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "addons/includes/master-css.php";?>   
    <?php include "addons/includes/master-js.php";?>   
    <?php include "addons/includes/sample-css.php";?>   
    <title>Document</title>
</head>
    <body>
        <div id="nav" class="navbar">
            <div class="modal-header">
                <h4>SOLO<span>Leveling</span></h4>
                <select class="form-control col-md-2" id="diff">
                    <option value="1" selected>Beginner</option>
                    <option value="2">Apprentice</option>
                    <option value="3">Master</option>
                    <option value="4">Legend</option>
                </select>
                <div class=""> 
                    <button id="btn-reset" class="btn btn-success mt-2" onclick="reset()">Reset</button>
                    <button id="btn-pause" class="btn btn-success mt-2" onclick="pause()">Pause</button>
                    <button id="btn-start" class="btn btn-primary mt-2" onclick="start()">Start</button>
                </div>
            </div>
            <div class="row col-md-12">
                <input id="timer-num" disabled type="text">
                <div class="pb-con">
                    <div id="timer"></div>
                </div>
            </div>
        </div>
        <div class="display-container">
            <div class="status">
                <input class="form-control col-md-2" id="points" disabled type="text" value="Points: 0">
                <input class="form-control col-md-2" id="level" disabled type="text" value="Level: 1">
                <input class="quest form-control" id="questions" disabled type="text" value="Loading Quest...">
            </div>
            <div class="quest">

            </div>
        </div>
        <div class="display">
            <div class="modal-header">
                <div class="btn-group col-md-12">
                    <input autofocus placeholder="0" class="form-control" type="number" id="answer">
                    <button id="btn-submit" onclick="submit()" type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
            <div class="modal-footer">
                <p><i>Disclaimer: This is only a simple math game, and is not intended for production, if you find any errors it is subject for improvement, thank you and enjoy.<br>Copyright © 2021 PID, All Rights Reserved.</i></p>
            </div>
        </div>
        <hr>
        <style>
            #timer{
                background: #0045a0;
                height: 50px;
            }
            .status{
                padding-left: 15px;
            }
            .status #points{
                margin-bottom: 5px;
            }
            #timer-num{
                width: 8.5%;
                text-align: center;
            }
            .quest{
                margin-left: 17%;
                width: 80%;
                margin-top: -81px;
                height: 82px;
                text-align: center;
                font-size: 30px;
                font-family: cursive;
            }
            .pb-con{
                width: 91.5%;
                border: solid 1px gray;
            }
            body{
                background-color: rgb(7, 7, 37) !important;
                margin: 0;
                padding: 0;
                font-family: 'Lucida Sans';
            }
            .display{
                margin: auto;
                margin-top: 60px;
                width: 450px;
            }
            input:disabled{
                color: black;
            }
            select{
                color: black !important;
            }
            .modal-header{
                width: 100% !important;
                border: none;
            }
            .modal-footer{
                color: #fff;
            }
            h4{
                text-align: left;
                padding-top: -10px;
                padding-bottom: 10px;
                color: #fff !important;
            }
            h4 span{
                margin-left: 2px;
                color: red;
            }

        </style>
        <?php include "addons/includes/master-js.php";?>
        <script>
            toastr.options = {
                    "positionClass": "toast-bottom-right",
                };

            var speed = 500;
            var decrementSpeed = 50;
            var p = 0;
            var l = 1;
            var i = 0;
            
            var q1 = 0;
            var q2 = 0;
            var ans = 0;
            var op = ['+','-'];
            var opDisp = "";
            var pauseDisp = "none";
            var startDisp = "";
            var submitDisp = "none";
            var bol = false;
            var diff = document.getElementById("diff");
            var btnStart = document.getElementById("btn-start");
            var btnPause = document.getElementById("btn-pause");
            var btnSubmit = document.getElementById("btn-submit");
            var timer = document.getElementById("timer");
            var timerNum = document.getElementById("timer-num");
            var questions = document.getElementById("questions");
            var answer = document.getElementById("answer");
            var points = document.getElementById("points");
            var level = document.getElementById("level");
            var answer = document.getElementById("answer");
            setInterval(hello, speed);
            function hello(){
                btnPause.style.display = pauseDisp;
                btnStart.style.display = startDisp;
                btnSubmit.style.display = submitDisp;
                points.value = "Points: " + p;
                level.value = "Quest: " + l;
                timer.style.width = i+"%";
                timerNum.value = i + " %";
                if(i == 101){
                    alert("GAME OVER YOU GOT " + p + " POINTS.");
                    reset();
                }else{
                    if(bol == true){
                        i++;
                    }
                }
            }
            function start(){
                if(bol == false){
                    bol = true;
                }
                startDisp = "none";
                pauseDisp = "";
                submitDisp = "";
                diff.style.display = "none";
                quest();
            }
            function pause(){
                alert("Game is paused, click OK to resume.");
                setTimeout(hello, 100);
            }

            function reset(){
                questions.value = "Loading Quest...";
                startDisp = "";
                submitDisp = "none";
                pauseDisp = "none";
                diff.style.display = "";
                bol = false;
                i = 0;
                p = 0;
                l = 1;
                q1 = 0;
                q2 = 0;
                ans = 0;
                opDisp = "";
            }

            function quest(){
                fetch('../addons/game_support/game.php', {
                    method: 'POST',
                    body: new URLSearchParams('diff=' + diff.value)
                })
                .then(res => res.json())
                .then(res => assignVal(res))
            }

            function assignVal(data){
                questions.value = "";
                q1 = data[0].q1;
                q2 = data[0].q2;
                opDisp = data[0].op;
                if(opDisp == "-"){
                    ans = (parseInt(q1) - parseInt(q2));
                }
                if(opDisp == "+"){
                    ans = (parseInt(q1) + parseInt(q2));
                }
                questions.value = q1 + opDisp + q2;
            }

            function submit(){
                var ansInput = answer.value;
                if(ansInput == ans){
                    p++;
                    l++;

                    if(l == 10){
                        decrementSpeed = (parseInt(decrementSpeed) - 10);
                    }
                    if(l == 20){
                        decrementSpeed = (parseInt(decrementSpeed) - 10);
                    }
                    if(l == 30){
                        decrementSpeed = (parseInt(decrementSpeed) - 10);
                    }
                    if(l == 40){
                        decrementSpeed = (parseInt(decrementSpeed) - 10);
                    }
                    if(l == 50){
                        decrementSpeed = 5;
                    }

                    i = i - decrementSpeed;
                    if(i < 0){
                        i = 0;
                    }
                    answer.value = "";
                    toastr.success("Correct.");

                    q1 = 0;
                    q2 = 0;
                    opDisp = "";

                    quest();

                }else{
                    if(ans == ""){
                        toastr.success("Click start button to start the game.");
                    }else{
                        toastr.info("Incorrect answer.");
                        answer.value = "";
                        quest();
                    }
                }
            }
        </script>
    </body>
</html>