<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?>  
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?> 
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-group'></span> Officers</h6>
                </div>
                <br>
                <table class="table-hover">
                    <thead>
                        <tr>
                            <td>Profile</td>
                            <td>Name</td>
                            <td>Email Address</td>
                            <td>Phone Number</td>
                            <td>Position</td>
                            <td style="width: 180px">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($officers as $officer)
                            <?php if($officer->position != "Administrator"){?>
                                <tr>
                                    <td class="pl-3 table-profile"><img src="../../addons/assets/root/profiles/{{ $officer->profile }}"></td>
                                    <td class="pl-3">{{ $officer->lname }}, {{ $officer->fname }} {{ $officer->mi }}.</td>
                                    <td>{{ $officer->email }}</td>
                                    <td>{{ $officer->phone }}</td>
                                    <td>{{ $officer->position }}</td>
                                    <td>
                                        <button data-toggle="modal" data-target="#viewProfile{{ $officer->id }}" class="btn btn-success btn-sm"><span class="fa fa-user"></span> Profile</button>
                                        <button data-toggle="modal" data-target="#confirmRemove{{ $officer->id }}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span> Remove</button>
                                    </td>
                                </tr>
                            <?php }?>
                            
                        <div class="modal fade" aria-hidden="true" id="confirmRemove{{ $officer->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('officers.destroy', $officer->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-trash"></span> Confirm</h5>
                                        </div>
                                        <div class="modal-body">
                                            <h6>Account of the officer will also be deleted, this can't be undone. Do you want to proceed?</h6>
                                            <hr>
                                            <div class="form-group">
                                                <input value="Name: {{ $officer->lname }}, {{ $officer->fname }} {{ $officer->mi }}." type="text" class="form-control" readonly>
                                            </div>
                                            <div class="form-group">
                                                <input value="Position: {{ $officer->position }}" type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm ml-1"><span class="fa fa-remove"></span> Cancel</button>
                                            <button type="submit" class="btn btn-danger btn-sm ml-1"><span class="fa fa-trash"></span> Proceed</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" aria-hidden="true" id="viewProfile{{ $officer->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('officers.update', $officer->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-user"></span> Profile</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="officer-profile">
                                                    <img src="../../addons/assets/root/profiles/{{ $officer->profile }}" alt="">
                                                </div>
                                                <hr class="profile-bar">
                                                    <textarea disabled class="form-control bio" rows="3">{{ $officer->bio }}</textarea>
                                                <hr class="profile-bar">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label>Last Name</label>
                                                        <input type="hidden" value="{{ $officer->id }}" class="form-control" name="id">
                                                        <input required type="text" value="{{ $officer->lname }}" class="form-control" name="lname">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <label>First Name</label>
                                                        <input required type="text" value="{{ $officer->fname }}" class="form-control" name="fname">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label>MI</label>
                                                        <input required type="text" value="{{ $officer->mi }}" class="form-control" name="mi">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Address</label>
                                                    <input required type="email" value="{{ $officer->email }}" class="form-control" name="email">
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <label>Phone No.</label>
                                                        <input required type="number" value="{{ $officer->phone }}" class="form-control" name="phone">
                                                    </div>
                                                    <div class="col-md-7">
                                                        <label>Position</label>
                                                        <input required type="text" value="{{ $officer->position }}" class="form-control" name="position">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            <?php include "addons/navigations/admin-footer.php";?>
        </div>
        <?php include "addons/includes/master-js.php";?>
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "officer-updated"){
            ?>
                toastr.info("Officer's information was updated.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script>  
    </body>
</html>