<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?>  
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?>   
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-calendar'></span> Meetings</h6>
                    <div>
                        <button data-toggle="modal" data-target="#createMeeting" class="btn btn-sm"><span class="fa fa-plus"></span> Create New Meeting</button>
                    </div>
                </div>
                <br>

                <div class="modal fade" aria-hidden="true" id="createMeeting">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('meetings.store') }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h5><span class="fa fa-list"></span> Create New Meeting</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>What</label>
                                        <input required type="text" class="form-control" name="what">
                                    </div>
                                    <div class="form-group">
                                        <label>Who</label>
                                        <select name="who" class="form-control">
                                            <option value="Core officers only">Core officers only</option>
                                            <option value="Year level officers only">Year level officers only</option>
                                            <option value="Class representatives only">Class representatives only</option>
                                            <option value="Core officers and Year level officers only">Core officers and Year level officers only</option>
                                            <option value="Core officers and Class representatives only">Core officers and Class representatives only</option>
                                            <option value="Year level officers and Class representatives only">Year level officers and Class representatives only</option>
                                            <option value="All officers">All officers</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Where</label>
                                        <input required type="text" class="form-control" name="where">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                        <?php $month = array('null','January','February','March','April','May','June','July','August','September','October','November','December')?>
                                        <?php $m = 1; $d = 1; $y = 2021;?>
                                            <div class="col-md-4">
                                                <label>Month</label>
                                                <select name="month" class="form-control">
                                                    <?php while($m <= 12){?>
                                                        <option value="<?php echo $m;?>"><?php echo $month[$m];?></option>
                                                    <?php $m++; }?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Day</label>
                                                <select name="day" class="form-control">
                                                    <?php while($d <= 31){?>
                                                        <option value="<?php echo $d;?>"><?php if($d < 10){ echo "0".$d;}else{echo $d;};?></option>
                                                    <?php $d++; }?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Year</label>
                                                <select name="year" class="form-control">
                                                    <?php while($y <= 2100){?>
                                                        <option value="<?php echo $y;?>"><?php echo $y;?></option>
                                                    <?php $y++; }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php $t = 0;?>
                                        <label>Military Time</label>
                                        <select name="time" class="form-control">
                                            <?php while($t <= 23){?>
                                                <option value="<?php if($t < 10){ echo "0".$t.":00"; }else{ echo $t.":00"; }?>"><?php if($t < 10){ echo "0".$t.":00"; }else{ echo $t.":00"; }?></option>
                                                <option value="<?php if($t < 10){ echo "0".$t.":30"; }else{ echo $t.":30"; }?>"><?php if($t < 10){ echo "0".$t.":30"; }else{ echo $t.":30"; }?></option>
                                            <?php $t++; }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                    <button class="btn btn-success btn-sm" type="submit"><span class="fa fa-save"></span> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <table class="table-hover">
                    <thead>
                        <tr>
                            <td style="width: 350px;">What</td>
                            <td style="width: 210px;">When</td>
                            <td>Where</td>
                            <td>Status</td>
                            <td style="width: 180px">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($meetings as $meeting)
                        <tr>
                            <td class="pl-3">{{ $meeting->what }}</td>
                            <td>{{ date('M d, Y @H:i', strtotime($meeting->when)) }}</td>
                            <td>{{ $meeting->where }}</td>
                            <td>{{ $meeting->status }}</td>
                            <td>
                                <button data-toggle="modal" data-target="#meetingDetails{{ $meeting->id }}" class="btn btn-success btn-sm"><span class="fa fa-info"></span> Details</button>
                                <button data-toggle="modal" data-target="#deleteMeeting{{ $meeting->id }}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span> Delete</button>
                            </td>
                        </tr>

                        <div class="modal fade" aria-hidden="true" id="meetingDetails{{ $meeting->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('meetings.update', $meeting->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-edit"></span> Meeting Details</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>What</label>
                                                <input value="{{ $meeting->what }}" required type="text" class="form-control" name="what">
                                            </div>
                                            <div class="form-group">
                                                <label>Who</label>
                                                <select name="who" class="form-control">
                                                    <option selected value="{{ $meeting->who }}">{{ $meeting->who.'*' }}</option>
                                                    <option value="Core officers only">Core officers only</option>
                                                    <option value="Year level officers only">Year level officers only</option>
                                                    <option value="Class representatives only">Class representatives only</option>
                                                    <option value="Core officers and Year level officers only">Core officers and Year level officers only</option>
                                                    <option value="Core officers and Class representatives only">Core officers and Class representatives only</option>
                                                    <option value="Year level officers and Class representatives only">Year level officers and Class representatives only</option>
                                                    <option value="All officers">All officers</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Where</label>
                                                <input required value="{{ $meeting->where }}" type="text" class="form-control" name="where">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                <?php $month = array('null','January','February','March','April','May','June','July','August','September','October','November','December')?>
                                                <?php $m = 1; $d = 1; $y = 2021;?>
                                                <?php
                                                    $indexmonth = 0;
                                                    if(date('m', strtotime($meeting->when)) < 10){
                                                        $tempmonth = date('m', strtotime($meeting->when));
                                                        $tempmonth = str_split($tempmonth, 1);
                                                        $indexmonth = $tempmonth[1];
                                                    }
                                                    else{
                                                        $tempmonth = date('m', strtotime($meeting->when));
                                                        $indexmonth = $tempmonth;
                                                    }
                                                    
                                                ?>
                                                    <div class="col-md-4">
                                                        <label>Month</label>
                                                        <select name="month" class="form-control">
                                                            <option selected value="<?php echo $indexmonth;?>"><?php echo $month[$indexmonth].'*';?></option>
                                                            <?php while($m <= 12){?>
                                                                <option value="<?php echo $m;?>"><?php echo $month[$m];?></option>
                                                            <?php $m++; }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Day</label>
                                                        <select name="day" class="form-control">
                                                            <option selected value="{{ date('d', strtotime($meeting->when)) }}">{{ date('d', strtotime($meeting->when)).'*' }}</option> 
                                                            <?php while($d <= 31){?>
                                                                <option value="<?php echo $d;?>"><?php if($d < 10){ echo "0".$d;}else{echo $d;};?></option>
                                                            <?php $d++; }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Year</label>
                                                        <select name="year" class="form-control">
                                                            <option selected value="{{ date('Y', strtotime($meeting->when)) }}">{{ date('Y', strtotime($meeting->when)).'*' }}</option> 
                                                            <?php while($y <= 2100){?>
                                                                <option value="<?php echo $y;?>"><?php echo $y;?></option>
                                                            <?php $y++; }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?php $t = 0;?>
                                                <label>Military Time</label>
                                                <select name="time" class="form-control">
                                                    <?php while($t <= 23){?>
                                                        <option selected value="{{ date('H:i', strtotime($meeting->when)) }}">{{ date('H:i', strtotime($meeting->when)).'*' }}</option>
                                                        <option value="<?php if($t < 10){ echo "0".$t.":00"; }else{ echo $t.":00"; }?>"><?php if($t < 10){ echo "0".$t.":00"; }else{ echo $t.":00"; }?></option>
                                                        <option value="<?php if($t < 10){ echo "0".$t.":30"; }else{ echo $t.":30"; }?>"><?php if($t < 10){ echo "0".$t.":30"; }else{ echo $t.":30"; }?></option>
                                                    <?php $t++; }?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Meeting Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="{{ $meeting->status }}">{{ $meeting->status.'*' }}</option>
                                                    <option value="Pending">Pending</option>
                                                    <option value="Ongoing">Ongoing</option>
                                                    <option value="Adjourned">Adjourned</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button class="btn btn-success btn-sm" type="submit"><span class="fa fa-save"></span> Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" aria-hidden="true" id="deleteMeeting{{ $meeting->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('meetings.destroy', $meeting->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-trash"></span> Confirm</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <h6>This can't be undone, do you want to proceed?</h6>
                                            </div>
                                            <div class="form-group">
                                                <input readonly value="{{ 'What: '.$meeting->what }}" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button class="btn btn-danger btn-sm" type="submit"><span class="fa fa-trash"></span> Proceed</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <?php include "addons/navigations/admin-footer.php";?>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "meeting-updated"){
            ?>
                toastr.info("Meeting updated successfuly.");
            <?php
                }
                if(session('toastType') == "meeting-added"){
            ?>
                toastr.info("Meeting created successfuly.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script>  
    </body>
</html>