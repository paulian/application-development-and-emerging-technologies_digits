<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?>   
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?> 
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
            <div class="modal-header" style="border-left: solid white 1px">
                <h6><span class='fa fa-envelope'></span> Messages</h6>
                <h6><?php if($unread > 0){ if($unread != 1){ echo $unread." unread messages"; }else{ echo $unread." unread message"; }}?></h6>
            </div>
            <div class="display message-list">
                <div class="modal-body">
                    @foreach ($mails as $mail)
                        @if($mail['uid'] != session('admin-uid'))
                            <a href="{{ route('open-message.show', $mail['uid']) }}">
                                <div class="form-group">
                                    <div class="modal-header dark-theme" style="background: white; color: #383838; padding: 5px;">
                                        <div style="width: 100%;">
                                            <h6 class="m-list <?php if($mail['read'] == "unread"){ echo 'dark-unread unread';}?>"><span class="fa fa-user"></span> <b class="mr-2">{{ $mail['name'] }}:</b> {{ $mail['position'] }}</h6>
                                            <h6 class="m-list <?php if($mail['read'] == "unread"){ echo 'dark-unread unread';}?>"><span class="fa fa-envelope"></span> {{ $mail['message'] }}</h6>
                                        </div>
                                        <div>
                                            <h6 class="t-list <?php if($mail['read'] == "unread"){ echo 'dark-unread unread';}?>"><i>{{ date('M d, Y - l @ H:i', strtotime($mail['time'])) }}</i></h6>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
                <?php include "addons/navigations/admin-footer.php";?>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>   
    </body>
</html>