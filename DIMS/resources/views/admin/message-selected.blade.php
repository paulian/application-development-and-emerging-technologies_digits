<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?>   
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?>  
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
            <div class="modal-header" style="border-left: solid white 1px">
                <h6><img class="ms-profile" src="../../addons/assets/root/profiles/{{ $officerprofile }}"> {{ $officerName }}: {{ $officerPosition }}</h6>
            </div>
            <div class="display message-list">
                <div class="modal-body">
                    <h6><span class=''></span> You are currently logged in as an <b>Administrator.</b></h6>
                    <div class="message-board" id="scrolling_to_bottom">
                        @foreach($mails as $mail)
                            <?php 
                                if($mail['sender'] == session('admin-uid')){
                                    if($mail['type'] != "message"){
                                        if($mail['type'] == "fa fa-file-photo-o"){
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <a href="../../addons/assets/root/mfiles/'.$mail['file'].'"><img class="message-img" src="../../addons/assets/root/mfiles/'.$mail['file'].'" alt=""></a>
                                                    </div>
                                                </div>
                                            ';
                                        }else if($mail['type'] == "fa fa-file-video-o"){
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <video class="message-img" controls="" name="media"><source src="../../addons/assets/root/mfiles/'.$mail['file'].'" type="audio/mpeg"></video>
                                                    </div>
                                                </div>
                                            ';
                                        }else{
                                            echo '
                                                <div class="r-con">
                                                    <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <a class="download-link" href="../../addons/file_download_support/downloadFile.php?file='.$mail['file'].'&name='.$mail['message'].'">
                                                            <div class="btn-group col-md-7">
                                                                <button class="btn btn-success '.$mail['type'].'" ></button>
                                                                <input disabled value="'.$mail['message'].'" class="form-control">
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                    }
                                    else{
                                        echo '
                                            <div class="r-con">
                                                <label><span class="fa fa-user"></span> <b>You:</b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                <div class="form-group">
                                                    <button class="btn btn-success">'.$mail['message'].'</button>
                                                </div>
                                            </div>
                                        ';
                                    }
                                }
                                elseif($mail['sender'] == session('user-uid')){
                                    if($mail['type'] != "message"){
                                        if($mail['type'] == "fa fa-file-photo-o"){
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>'.session('officerName').': </b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <a href="../../addons/assets/root/mfiles/'.$mail['file'].'"><img class="message-img" src="../../addons/assets/root/mfiles/'.$mail['file'].'" alt=""></a>
                                                    </div>
                                                </div>
                                            ';
                                        }else if($mail['type'] == "fa fa-file-video-o"){
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>'.session('officerName').': </b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <video class="message-img" controls="" name="media"><source src="../../addons/assets/root/mfiles/'.$mail['file'].'" type="audio/mpeg"></video>
                                                    </div>
                                                </div>
                                            ';
                                        }else{
                                            echo '
                                                <div class="l-con">
                                                    <label><span class="fa fa-user"></span> <b>'.session('officerName').': </b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                    <div class="form-group">
                                                        <a class="download-link" href="../../addons/file_download_support/downloadFile.php?file='.$mail['file'].'&name='.$mail['message'].'">
                                                            <div class="btn-group col-md-7">
                                                                <input disabled value="'.$mail['message'].'" class="form-control">
                                                                <button class="btn btn-dark '.$mail['type'].'" ></button>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            ';
                                        }
                                    }
                                    else{
                                        echo '
                                            <div class="l-con">
                                                <label><span class="fa fa-user"></span> <b>'.session('officerName').': </b> <i>'.date('M d, Y - l @ H:i', strtotime($mail['time'])).'</i></label>
                                                <div class="form-group">
                                                    <button class="btn btn-dark">'.$mail['message'].'</button>
                                                </div>
                                            </div>
                                        ';
                                    }
                                }
                            ?>
                        @endforeach
                    </div>
                    <form action="{{ route('message-selected.store') }}" method="POST">               
                        @csrf
                        @method('POST')
                        <div class="send-board">
                            <button data-toggle="modal" data-target="#browseFile" type="button" class="btn btn-success"><span class="fa fa-plus"></span></button>
                            <input autofocus="true" placeholder="Aa" required name="message" type="text" class="form-control">
                            <button type="submit" class="btn btn-success"><span class="fa fa-send"></span></button>
                        </div>
                    </form>
                </div>
                <?php include "addons/navigations/admin-footer.php";?>
            </div>
            <div class="modal fade" id="browseFile" area-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    </div>
                    <form action="../addons/file_upload_support/uploadfile.php?sen={{ session('admin-uid') }}&rec={{ session('user-uid') }}&command=admin" method="POST" enctype="multipart/form-data">
                        <div class="modal-body" style="padding: 10px;">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="new_file" id="upload" onchange="displayname(this,$(this))">
                                        <label class="custom-file-label" for="upload">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Save</button>
                            <button data-dismiss="modal" class="btn btn-dark btn-sm"><span class="fa fa-times"></span> Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>   
        <script>
            $('#scrolling_to_bottom').animate({
                scrollTop: $('#scrolling_to_bottom').get(0).scrollHeight}, 0);
        </script>
    </body>
</html>