<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?>  
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?>   
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-flag'></span> Events</h6>
                    <div>
                        <button data-toggle="modal" data-target="#createEvent" class="btn btn-sm"><span class="fa fa-plus"></span> Create Event</button>
                    </div>
                </div>

                <div class="modal fade" aria-hidden="true" id="createEvent">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('events.store') }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h5><span class="fa fa-list"></span> Create New Event</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Event Name</label>
                                        <input required type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label>Event Theme (Optional)</label>
                                        <input type="text" class="form-control" name="theme">
                                    </div>
                                    <div class="form-group">
                                        <label>Event Venue</label>
                                        <input required type="text" class="form-control" name="venue">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                        <?php $month = array('null','January','February','March','April','May','June','July','August','September','October','November','December')?>
                                        <?php $m = 1; $d = 1; $y = 2021;?>
                                            <div class="col-md-4">
                                                <label>Month</label>
                                                <select name="month" class="form-control">
                                                    <?php while($m <= 12){?>
                                                        <option value="<?php echo $m;?>"><?php echo $month[$m];?></option>
                                                    <?php $m++; }?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Day</label>
                                                <select name="day" class="form-control">
                                                    <?php while($d <= 31){?>
                                                        <option value="<?php echo $d;?>"><?php if($d < 10){ echo "0".$d;}else{echo $d;};?></option>
                                                    <?php $d++; }?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Year</label>
                                                <select name="year" class="form-control">
                                                    <?php while($y <= 2100){?>
                                                        <option value="<?php echo $y;?>"><?php echo $y;?></option>
                                                    <?php $y++; }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Event Organizers (Required atleast 1)</label>
                                        <br>
                                        <div class="btn-group col form-group" style="padding: 0;">
                                            <button disabled class="btn btn-dark">1</button>
                                            <input required type="text" class="form-control" name="organizer1">
                                        </div>
                                        <div class="btn-group col form-group" style="padding: 0;">
                                            <button disabled class="btn btn-dark">2</button>
                                            <input type="text" class="form-control" name="organizer2">
                                        </div>
                                        <div class="btn-group col form-group" style="padding: 0;">
                                            <button disabled class="btn btn-dark">3</button>
                                            <input type="text" class="form-control" name="organizer3">
                                        </div>
                                        <div class="btn-group col form-group" style="padding: 0;">
                                            <button disabled class="btn btn-dark">4</button>
                                            <input type="text" class="form-control" name="organizer4">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                    <button class="btn btn-success btn-sm" type="submit"><span class="fa fa-save"></span> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br>
                <table class="table-hover">
                    <thead>
                        <tr>
                            <td style="width: 400px;">Event Name</td>
                            <td style="width: 220px;">Date</td>
                            <td>Venue</td>
                            <td style="width: 180px;">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($events as $event)
                        <tr>
                            <td class="pl-3">{{ $event->name }}</td>
                            <td>{{ date('M d, Y - l ', strtotime($event->date)) }}</td>
                            <td>{{ $event->venue }}</td>
                            <td>
                                <button data-toggle="modal" data-target="#eventDetails{{ $event->id }}" class="btn btn-success btn-sm"><span class="fa fa-info"></span> Details</button>
                                <button data-toggle="modal" data-target="#deleteEvent{{ $event->id }}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span> Delete</button>
                            </td>
                        </tr>

                        <div class="modal fade" aria-hidden="true" id="eventDetails{{ $event->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('events.update', $event->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-edit"></span> Event Details</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Event Name</label>
                                                <input required value="{{ $event->name }}" type="text" class="form-control" name="name">
                                            </div>
                                            <div class="form-group">
                                                <label>Event Theme (Optional)</label>
                                                <input type="text" value="{{ $event->theme }}" class="form-control" name="theme">
                                            </div>
                                            <div class="form-group">
                                                <label>Event Venue</label>
                                                <input required  value="{{ $event->venue }}" type="text" class="form-control" name="venue">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                <?php $month = array('null','January','February','March','April','May','June','July','August','September','October','November','December')?>
                                                <?php $m = 1; $d = 1; $y = 2021;?>
                                                <?php
                                                    $indexmonth = 0;
                                                    if(date('m', strtotime($event->date)) < 10){
                                                        $tempmonth = date('m', strtotime($event->date));
                                                        $tempmonth = str_split($tempmonth, 1);
                                                        $indexmonth = $tempmonth[1];
                                                    }
                                                    else{
                                                        $tempmonth = date('m', strtotime($event->date));
                                                        $indexmonth = $tempmonth;
                                                    }
                                                    
                                                ?>
                                                    <div class="col-md-4">
                                                        <label>Month</label>
                                                        <select name="month" class="form-control">
                                                            <option selected value="<?php echo $indexmonth;?>"><?php echo $month[$indexmonth];?></option>
                                                            <?php while($m <= 12){?>
                                                                <option value="<?php echo $m;?>"><?php echo $month[$m];?></option>
                                                            <?php $m++; }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Day</label>
                                                        <select name="day" class="form-control">
                                                            <option selected value="{{ date('d', strtotime($event->date)) }}">{{ date('d', strtotime($event->date)) }}</option> 
                                                            <?php while($d <= 31){?>
                                                                <option value="<?php echo $d;?>"><?php if($d < 10){ echo "0".$d;}else{echo $d;};?></option>
                                                            <?php $d++; }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Year</label>
                                                        <select name="year" class="form-control">
                                                            <option selected value="{{ date('Y', strtotime($event->date)) }}">{{ date('Y', strtotime($event->date)) }}</option> 
                                                            <?php while($y <= 2100){?>
                                                                <option value="<?php echo $y;?>"><?php echo $y;?></option>
                                                            <?php $y++; }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Event Organizers (Required atleast 1)</label>
                                                <br>
                                                <div class="btn-group col form-group" style="padding: 0;">
                                                    <button disabled class="btn btn-dark">1</button>
                                                    <input required value="{{ $event->organizer1 }}" type="text" class="form-control" name="organizer1">
                                                </div>
                                                <div class="btn-group col form-group" style="padding: 0;">
                                                    <button disabled class="btn btn-dark">2</button>
                                                    <input type="text" value="{{ $event->organizer2 }}" class="form-control" name="organizer2">
                                                </div>
                                                <div class="btn-group col form-group" style="padding: 0;">
                                                    <button disabled class="btn btn-dark">3</button>
                                                    <input type="text" value="{{ $event->organizer3 }}" class="form-control" name="organizer3">
                                                </div>
                                                <div class="btn-group col form-group" style="padding: 0;">
                                                    <button disabled class="btn btn-dark">4</button>
                                                    <input type="text" value="{{ $event->organizer4 }}" class="form-control" name="organizer4">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button class="btn btn-success btn-sm" type="submit"><span class="fa fa-save"></span> Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" aria-hidden="true" id="deleteEvent{{ $event->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('events.destroy', $event->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-trash"></span> Confirm</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <h6>This can't be undone, do you want to proceed?</h6>
                                                <hr>
                                            </div>
                                            <div class="form-group">
                                                <label>Event Name</label>
                                                <input readonly value="{{ $event->name }}" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button class="btn btn-danger btn-sm" type="submit"><span class="fa fa-trash"></span> Proceed</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                <?php include "addons/navigations/admin-footer.php";?>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "event-updated"){
            ?>
                toastr.info("Event was updated.");
            <?php
                }
                if(session('toastType') == "event-added"){
            ?>
                toastr.info("Event created successfuly.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script>  
    </body>
</html>