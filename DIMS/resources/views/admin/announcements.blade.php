<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include "addons/includes/master-css.php";?>   
        <?php include "addons/includes/master-js.php";?>   
        <?php include "addons/includes/admin-css.php";?> 
        <?php if(session('admin-theme') == 'dark'){ include "addons/includes/admin-dark-css.php"; }?>   
        <title>DIGITS Information Management System</title>
    </head>
    <body>
        <?php include "addons/navigations/admin-navbar.php";?>
        <?php include "addons/navigations/admin-sidebar.php";?>
        <div class="display-container">
                <div class="modal-header" style="border-left: solid white 1px">
                    <h6><span class='fa fa-bell'></span> Announcements</h6>
                    <div>
                        <button data-toggle="modal" data-target="#addAnnouncement" class="btn btn-sm"><span class="fa fa-plus"></span> Add Announcement</button>
                    </div>
                </div>
                <br>

                <div class="modal fade" aria-hidden="true" id="addAnnouncement">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{ route('announcements.store' ) }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="modal-header">
                                    <h5><span class="fa fa-list"></span> Create New Announcement</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Announcer</label>
                                        <input required type="text" class="form-control" name="announcer">
                                    </div>
                                    <div class="form-group">
                                        <label>Announcement</label>
                                        <textarea rows="10" required class="form-control" name="details"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                    <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <table class="table-hover">
                    <tbody>
                        @foreach ($announcements as $announcement)
                        <tr style="display: flex; justify-content: space-between; border-bottom: solid 1px gray;">
                            <td class="ml-3" style="width: 850px;">
                                <h6><span class="fa fa-user"></span> {{ $announcement->announcer }} : {{ date('M d, Y - h:i A ', strtotime($announcement->created_at)) }}</h6>
                                <div class="ml-4">{{ $announcement->details }}</div>
                            </td>
                            <td>
                                <button data-toggle="modal" data-target="#announcementDetails{{ $announcement->id }}" class="btn btn-success btn-sm"><span class="fa fa-info"></span> Details</button>
                                <button data-toggle="modal" data-target="#deleteAnnouncement{{ $announcement->id }}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span> Delete</button>
                            </td>
                        </tr>

                        <div class="modal fade" aria-hidden="true" id="announcementDetails{{ $announcement->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('announcements.update', $announcement->id ) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-edit"></span> Announcement Details</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Announcer</label>
                                                <input value="{{ $announcement->announcer }}" required type="text" class="form-control" name="announcer">
                                            </div>
                                            <div class="form-group">
                                                <label>Announcement</label>
                                                <textarea rows="10" required class="form-control" name="details">{{ $announcement->details }}</textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button type="submit" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" aria-hidden="true" id="deleteAnnouncement{{ $announcement->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{ route('announcements.destroy', $announcement->id ) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-header">
                                            <h5><span class="fa fa-trash"></span> Confirm</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <h6>This can't be undone, do you want to proceed?</h6>
                                            </div>
                                            <div class="form-group">
                                                <label>Announcer</label>
                                                <input readonly value="{{ $announcement->announcer }}" required type="text" class="form-control" name="announcer">
                                            </div>
                                            <div class="form-group">
                                                <label>Announcement</label>
                                                <textarea readonly rows="5" required class="form-control" name="details">{{ $announcement->details }}</textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                                            <button type="submit" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span> Proceed</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            <?php include "addons/navigations/admin-footer.php";?>
            </div>
        </div>
        <?php include "addons/includes/master-js.php";?>  
        <script>
            toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            <?php
                if(session('toastType') == "announcement-updated"){
            ?>
                toastr.info("Announcemnet was updated.");
            <?php
                }
                if(session('toastType') == "announcement-added"){
            ?>
                toastr.info("Announcement added successfuly.");
            <?php
                }
                session()->put('toastType', '');
            ?>
        </script> 
    </body>
</html>