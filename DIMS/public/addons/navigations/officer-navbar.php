<div class="modal-header sticky-top officer-navbar">
    <h5><span class="fa fa-book"></span> DIGITS Information Management System</h5>
    <div>
        <a href="user-dashboard"><span class="fa fa-dashboard"></span> Dashboard</a>
        <a href="user-officers"><span class="fa fa-group"></span> Officers</a>
        <a href="user-message"><span class="fa fa-envelope"></span> Message</a>
        <a href="user-account"><span class="fa fa-gears"></span> Account</a>
        <a href="user-theme"><span class="fa fa-moon-o"></span> Dark Mode<?php if(session('user-theme') == 'default'){ echo ": Off"; }else{ echo ": On"; }?></a>
        <a href="user-signout"><span class="fa fa-sign-out"></span> Sign out</a>
    </div>
</div>