<div class="sidebar-container">
    <a <?php if(session('admin-location') == "dashboard"){ echo 'class="isActive"';}?> href="dashboard"><span class="fa fa-tachometer"></span> Dashboard</a>
    <a <?php if(session('admin-location') == "officers"){ echo 'class="isActive"';}?> href="officers"><span class="fa fa-group"></span> Officers</a>
    <a <?php if(session('admin-location') == "messages"){ echo 'class="isActive"';}?> href="messages"><span class="fa fa-envelope"></span> Messages</a>
    <a <?php if(session('admin-location') == "events"){ echo 'class="isActive"';}?> href="events"><span class="fa fa-flag"></span> Events</a>
    <a <?php if(session('admin-location') == "meetings"){ echo 'class="isActive"';}?> href="meetings"><span class="fa fa-calendar"></span> Mettings</a>
    <a <?php if(session('admin-location') == "announcements"){ echo 'class="isActive"';}?> href="announcements"><span class="fa fa-bell"></span> Announcements</a>
    <a href="theme"><span class="fa fa-moon-o"></span> Dark Mode<?php if(session('admin-theme') == 'default'){ echo ": Off"; }else{ echo ": On"; }?></a>
</div>