<div class="modal-header sticky-top">
    <h5><span class="fa fa-book"></span> DIGITS Information Management System</h5>
    <div>
        <a href="signout" class="btn btn-secondary btn-sm"><span class="fa fa-sign-out"></span> Sign out</a>
    </div>
</div>