<?php
    $diff = $_POST['diff'];
    $q1 = 0;
    $q2 = 0;
    $op = ['-','+'];

    if($diff == 1){
        $rmin = 1;
        $rmax = 100;
    }else if($diff == 2){
        $rmin = 101;
        $rmax = 500;
    }else if($diff == 3){
        $rmin = 501;
        $rmax = 1000;
    }else if($diff == 4){
        $rmin = 1001;
        $rmax = 10000;
    }
    
    $q1 = rand($rmin, $rmax);
    $q2 = rand($rmin, $rmax);
    $optemp = $op[rand(0, 1)];
    $data = array(["q1" => $q1, "q2" => $q2, "op" => $optemp]);
    echo json_encode($data);
?>