function displayname(input,_this) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.files[0]['size'] > 40000000){
                _this.siblings('label').html("File size too large.")
                toastr.warning("File size too large, max 40mb.");
            }
            else{
                _this.siblings('label').html(input.files[0]['name'])
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}


