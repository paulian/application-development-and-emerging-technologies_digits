<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Officers extends Model
{
    use HasFactory;
    protected $table = 'officers';
    protected $fillable = ['lname', 'fname', 'mi', 'email', 'phone', 'position', 'bio', 'profile'];
}
