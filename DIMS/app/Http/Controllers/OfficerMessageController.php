<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use App\Models\SendMessage;
use Illuminate\Http\Request;

class OfficerMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('officerId')){
            /**
             * Get all message
             */
            $userid = session('user-uid');
            $messages = SendMessage::where('chatid','=',$userid)->get();

            /**
            * Get data of currently login user
            */
            $id = session('officerId');
            $user = Officers::find($id);
            session()->put('user-location', 'message');
            return view('officer.message', compact('user','messages'));
        }else{
            return redirect()->route('login.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sendmessagedata = new SendMessage;
        $sendmessagedata->chatid = session('user-uid');
        $sendmessagedata->senderid = session('user-uid');
        $sendmessagedata->message = $request->input('message');
        $sendmessagedata->type = "message";
        $sendmessagedata->file = "null";
        $sendmessagedata->read = "unread";
        $sendmessagedata->save();

        return redirect()->route('user-message.index');
    }
}
