<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use App\Models\Events;
use App\Models\Meetings;
use App\Models\Announcements;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            /**
             * Dashboard statistics
             */
            $o = 0;
            $e = 0;
            $m = 0;

            $officers = Officers::all();
            $events = Events::all();
            $meetings = Meetings::all();

            foreach($officers as $officer) {
                if($officer->uid != session('admin-uid')){
                    $o++;
                }
            }

            foreach($events as $event) {
                $e++;
            }

            foreach($meetings as $meeting) {
                $m++;
            }

            /**
             * Dashboard announcements
             */
            $announcements = Announcements::all()->reverse();
            session()->put('admin-location', 'dashboard');
            return view('admin.dashboard', compact('o','e','m','announcements'));
        }else{
            return redirect()->route('login.index');
        }
        
    }
}
