<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use Illuminate\Http\Request;

class OfficerOfficersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('officerId')){
            /**
             * Get data of currently login user
             */
            $id = session('officerId');
            $user = Officers::find($id);

            /**
             * Get all user data
             */
            $officers = Officers::all();
            session()->put('user-location', 'officers');
            return view('officer.officers', compact('officers','user'));
            
        }else{
            return redirect()->route('login.index');
        }
        
    }
}
