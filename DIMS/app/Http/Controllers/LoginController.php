<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('officerId') || session()->has('admin-uid')){
            if(session()->has('officerId')){
                session()->forget('admin-uid');
                if(session('user-location') == 'dashboard'){
                    return redirect()->route('user-dashboard.index');
                }
                elseif(session('user-location') == 'officers'){
                    return redirect()->route('user-officers.index');
                }
                elseif(session('user-location') == 'message'){
                    return redirect()->route('user-message.index');
                }
                elseif(session('user-location') == 'account'){
                    return redirect()->route('user-account.index');
                }
                else{
                    return redirect()->route('user-dashboard.index');
                }
            }
            elseif(session()->has('admin-uid')){
                session()->forget('officerId');
                if(session('admin-location') == 'dashboard'){
                    return redirect()->route('dashboard.index');
                }
                elseif(session('admin-location') == 'officers'){
                    return redirect()->route('officers.index');
                }
                elseif(session('admin-location') == 'messages'){
                    return redirect()->route('messages.index');
                }
                elseif(session('admin-location') == 'meetings'){
                    return redirect()->route('meetings.index');
                }
                elseif(session('admin-location') == 'events'){
                    return redirect()->route('events.index');
                }
                elseif(session('admin-location') == 'announcements'){
                    return redirect()->route('announcements.index');
                }
                else{
                    return redirect()->route('dashboard.index');
                }
            }
        }else{
            return view('credentials.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth = "";
        $username = "";
        $password = "";
        $accountdatas = Officers::where('lname','=',$request->username,'and','email','=',$request->password)->get();
        
        foreach($accountdatas as $accountdata){
            $auth = $accountdata;
        }

        /**
         * Verify Data
         */
        if($auth != ""){
            session()->put('user-theme','default');
            session()->forget('admin-uid');
            $request->session()->put('officerId', $auth->id);
            $request->session()->put('user-uid', $auth->uid);
            return redirect('/officer/user-dashboard');
        }
        else{
            $username = md5($request->username);
            $password = md5($request->password);
            $accountdatas = Officers::where('lname','=',$username,'and','email','=',$password)->get();
            foreach($accountdatas as $accountdata){
                $auth = $accountdata;
            }

            if($auth != ""){
                session()->put('admin-theme','default');
                session()->forget('officerId');
                $request->session()->put('admin-uid', $auth->uid);
                return redirect('/admin/dashboard');
            }
            else{
                session()->put('toastType','password-warning');
                return redirect()->route('login.index');
            }
        }
    }
}
