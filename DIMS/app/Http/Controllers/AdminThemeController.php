<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(session()->has('admin-theme')){
            if(session('admin-theme') == 'default'){
                session()->put('admin-theme', 'dark');
                echo session('admin-theme');
                return redirect()->route('login.index');
            }
            else{
                session()->put('admin-theme', 'default');
                echo session('admin-theme');
                return redirect()->route('login.index');
            }
        }
        else{
            session()->put('admin-theme','default');
            return redirect()->route('theme.index');
        }
    }
}
