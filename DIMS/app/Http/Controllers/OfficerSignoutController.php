<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OfficerSignoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->forget('officerId');
        session()->forget('user-uid');
        session()->forget('toastType');
        session()->put('user-location');
        return redirect()->route('login.index');
    }
}