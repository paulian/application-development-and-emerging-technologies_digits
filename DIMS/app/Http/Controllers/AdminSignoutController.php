<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminSignoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->forget('admin-uid');
        session()->forget('toastType');
        session()->forget('admin-location');
        return redirect()->route('login.index');
    }
}
