<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use Illuminate\Http\Request;

class OfficerUploadProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->profileName == "filetypeerror"){
            session()->put('toastType', 'profile-invalid');
            return redirect()->route('user-account.index');
        }else{
            $id = $request->id;
            $profiledata = Officers::find($id);
            $profiledata->profile = $request->profileName;
            $profiledata->save();
            session()->put('toastType', 'profile-updated');
            return redirect()->route('user-account.index');
        }
        
    }
  }
