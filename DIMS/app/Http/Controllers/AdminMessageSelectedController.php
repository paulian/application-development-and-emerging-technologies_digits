<?php

namespace App\Http\Controllers;

use App\Models\SendMessage;
use Illuminate\Http\Request;

class AdminMessageSelectedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $id = session('user-uid');
            $messageid = 0;
            $index = 0;
            $mails = array();
            $messages = SendMessage::where('chatid','=',$id)->get();

            foreach($messages as $message){
                $mails[$index] = [
                    $messageid = $message->id,
                    'id' => $message->id,
                    'time' => $message->created_at,
                    'message' => $message->message,
                    'sender' => $message->senderid,
                    'type' => $message->type,
                    'file' => $message->file
                ];
                $index++;
            }

            $updatemessage = SendMessage::find($messageid);
            $updatemessage->read = "read";
            $updatemessage->save();

            

            $officerName = session('officerName');
            $officerPosition = session('officerPosition');
            $officerprofile = session('officerprofile');
            session()->put('admin-location', 'messages');
            return view('admin.message-selected', compact('mails','officerName','officerPosition','officerprofile'));
        }else{
            return redirect()->route('login.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sendmessagedata = new SendMessage;
        $sendmessagedata->chatid = session('user-uid');
        $sendmessagedata->senderid = session('admin-uid');
        $sendmessagedata->message = $request->input('message');
        $sendmessagedata->type = "message";
        $sendmessagedata->file = "null";
        $sendmessagedata->read = "read";
        $sendmessagedata->save();

        return redirect()->route('message-selected.index');
    }
}
