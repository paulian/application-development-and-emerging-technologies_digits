<?php

namespace App\Http\Controllers;

use App\Models\Meetings;
use App\Models\Announcements;
use Illuminate\Http\Request;

class AdminMeetingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $meetings = Meetings::all()->reverse();
            session()->put('admin-location', 'meetings');
            return view('admin.meetings', compact('meetings'));
        }else{
            return redirect()->route('login.index');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meetingdata = new Meetings;
        $meetingdata->what = $request->input('what');
        $meetingdata->where = $request->input('where');
        $meetingdata->who = $request->input('who');
        $meetingdata->when = $request->input('year').'-'.$request->input('month').'-'.$request->input('day')." ".$request->time.":00";
        $meetingdata->status = "Pending";
        $meetingdata->save();

        /**
         * This is for announcement purpose of the meeting
         */

        $announcementdata = new Announcements;
        $announcementdata->announcer = "Admin";
        $announcementdata->details = 'MEETING | '.'What: '.$request->what.' / '.'Who: '.$request->who.' / '.'Where: '.$request->where.' / '.'When: '.date('M d, Y - l @ H:i', strtotime($meetingdata->when)).' / '.'Status: '.$meetingdata->status.' / Please come on time, don\'t be late.'; 
        $announcementdata->save();
        session()->put('toastType', 'meeting-added');
        return redirect()->route('meetings.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meetingdata = Meetings::find($id);
        $meetingdata->what = $request->input('what');
        $meetingdata->where = $request->input('where');
        $meetingdata->who = $request->input('who');
        $meetingdata->when = $request->input('year').'-'.$request->input('month').'-'.$request->input('day')." ".$request->time.":00";
        $meetingdata->status = $request->status;
        $meetingdata->save();

        /**
         * This is for announcement purpose of the meeting
         */

        if($meetingdata->status != "Pending"){
            $announcementdata = new Announcements;
            $announcementdata->announcer = "Admin";
            $announcementdata->details = 'MEETING UPDATE | '.'What: '.$request->what.' / '.'Status: '.$meetingdata->status; 
            $announcementdata->save();
        }
        else{
            $announcementdata = new Announcements;
            $announcementdata->announcer = "Admin";
            $announcementdata->details = 'MEETING UPDATE | '.'What: '.$request->what.' / '.'Who: '.$request->who.' / '.'Where: '.$request->where.' / '.'When: '.date('M d, Y - l @ H:i', strtotime($meetingdata->when)).' / '.'Status: '.$meetingdata->status.' / Please come on time, don\'t be late.'; 
            $announcementdata->save();
        }
        session()->put('toastType', 'meeting-updated');
        return redirect()->route('meetings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meetings $meeting)
    {
        $meeting->delete();
        return redirect()->route('meetings.index');
    }
}
