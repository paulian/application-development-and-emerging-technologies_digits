<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OfficerThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('user-theme')){
            if(session('user-theme') == 'default'){
                session()->put('user-theme', 'dark');
                echo session('user-theme');
                return redirect()->route('login.index');
            }
            else{
                session()->put('user-theme', 'default');
                echo session('user-theme');
                return redirect()->route('login.index');
            }
        }
        else{
            session()->put('user-theme','default');
            return redirect()->route('user-theme.index');
        }
    }
}
