<?php

namespace App\Http\Controllers;

use App\Models\Announcements;
use Illuminate\Http\Request;

class AdminAnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $announcements = Announcements::all()->reverse();
            session()->put('admin-location', 'announcements');
            return view('admin.announcements', compact('announcements'));
        }else{
            return redirect()->route('login.index');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Announcements::create($request->all());
        session()->put('toastType', 'announcement-added');
        return redirect()->route('announcements.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcements $announcement)
    {
        $announcement->update($request->all());
        session()->put('toastType', 'announcement-updated');
        return redirect()->route('announcements.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcements $announcement)
    {
        $announcement->delete();
        return redirect()->route('announcements.index');
    }
}
