<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use Illuminate\Http\Request;

class OfficerAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('officerId')){
            $id = session('officerId');
            $officer = Officers::find($id);
            session()->put('user-location', 'account');
            return view('officer.account', compact('officer'));
        }else{
            return redirect()->route('login.index');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $officerdata = Officers::find($id);
        $officerdata->lname = $request->input('lname');
        $officerdata->fname = $request->input('fname');
        $officerdata->mi = $request->input('mi');
        $officerdata->email = $request->input('email');
        $officerdata->phone = $request->input('phone');
        $officerdata->position = $request->input('position');
        $officerdata->bio = $request->input('bio');
        $officerdata->save();
        session()->put('toastType', 'account-updated');
        return redirect()->route('user-account.index');
    }
}
