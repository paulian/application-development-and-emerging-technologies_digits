<?php

namespace App\Http\Controllers;

use App\Models\SendMessage;
use App\Models\Officers;
use Illuminate\Http\Request;

class AdminMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $officers = Officers::all()->reverse();
            $messages = SendMessage::all();

            $index = 0;
            $mails = array();
            $tempmessageid = "";
            $tempmessage = "";
            $tempsenderid = "";
            $temptype = "";
            $tempmessagetime = "";
            $tempmessageread = "";

            foreach($officers as $officer){
                foreach($messages as $message){
                    if($message->chatid == $officer->uid){
                        $tempsenderid = $message->senderid;
                        $tempmessage = $message->message;
                        $temptype = $message->type;
                        $tempmessagetime = $message->created_at;
                        $tempmessageread = $message->read;
                    }
                }

                $mails[$index] = [
                    'id' => $officer->id,
                    'uid' => $officer->uid,
                    'name' => $officer->fname.' '.$officer->lname,
                    'position' => $officer->position,
                    'senderid' => $tempsenderid,
                    'message' => $tempmessage,
                    'type' => $temptype,
                    'time' => $tempmessagetime,
                    'read' => $tempmessageread
                ];
                $index++;
            }   

            //Sorting arrays for inbox displays the latest
            //store data in temporary array container from old array container($mails)
            $counter = 0;
            $sid = array();
            $suid = array();
            $sname = array();
            $sposition = array();
            $smessage = array();
            $stime = array();
            $sread = array();

            foreach($mails as $mail){
                $sid[$counter] = $mail['id'];
                $suid[$counter] = $mail['uid'];
                $sname[$counter] = $mail['name'];
                $sposition[$counter] = $mail['position'];
                if($mail['type'] != "message"){
                    $sentWhat = "an attachment.";
                    if($mail['type'] == 'fa fa-file-photo-o'){
                        $sentWhat = 'a photo.';
                    }else if($mail['type'] == 'fa fa-file-video-o'){
                        $sentWhat = 'a video.';
                    }else{
                        $sentWhat = "an attachment.";
                    }

                    if($mail['senderid'] == "admin-607add0a06445"){
                        $smessage[$counter] = 'You sent '.$sentWhat;
                    }else{
                        $smessage[$counter] = $mail['name'].' sent '.$sentWhat;
                    }
                }
                else{
                    $smessage[$counter] = $mail['message'];
                }
                $stime[$counter] = $mail['time'];
                $sread[$counter] = $mail['read'];
                $counter++;
            }
            
            //sort out array by time
            do{
                $swaped = false;
                /* -1 or -2 something fishy can't understand why message list is incomplete if I use -1*/
                for($i = 0, $c = count($stime)-2; $i < $c; $i++){
                    if($stime[$i] < $stime[$i + 1]){
                        list($stime[$i + 1], $stime[$i]) = array($stime[$i], $stime[$i + 1]);
                        list($suid[$i + 1], $suid[$i]) = array($suid[$i], $suid[$i + 1]);
                        list($sid[$i + 1], $sid[$i]) = array($sid[$i], $sid[$i + 1]);
                        list($sname[$i + 1], $sname[$i]) = array($sname[$i], $sname[$i + 1]);
                        list($sposition[$i + 1], $sposition[$i]) = array($sposition[$i], $sposition[$i + 1]);
                        list($smessage[$i + 1], $smessage[$i]) = array($smessage[$i], $smessage[$i + 1]);
                        list($sread[$i + 1], $sread[$i]) = array($sread[$i], $sread[$i + 1]);
                        $swaped = true;
                    }
                }
            }while($swaped);

            //restore sorted array to old array container
            $s = 0;
            $unread = 0;
            while($s < count($stime) - 1){
                //holds value for unread messages
                if($sread[$s] == "unread"){
                    $unread++;
                }
                $mails[$s] = [
                    'id' => $sid[$s],
                    'uid' => $suid[$s],
                    'name' => $sname[$s],
                    'position' => $sposition[$s],
                    'message' => $smessage[$s],
                    'time' => $stime[$s],
                    'read' => $sread[$s]
                ];
                $s++;
            }

            session()->put('admin-location', 'messages');
            return view('admin.messages', compact('mails','unread'));
        }else{
            return redirect()->route('login.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $officername = "";
        $officerposition = "";
        $officers = Officers::where('uid','=',$id)->get();

        foreach($officers as $officer){
            $officername = $officer->fname.' '.$officer->lname;
            $officerposition = $officer->position;
            $officerprofile = $officer->profile;
        }

        session()->put('user-uid', $id);
        session()->put('officerName', $officername);
        session()->put('officerPosition', $officerposition);
        session()->put('officerprofile', $officerprofile);

        return redirect()->route('message-selected.index', compact('officername'));
        
    }

}
