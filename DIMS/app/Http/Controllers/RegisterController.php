<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use App\Models\SendMessage;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registerdata = new Officers;
        $registerdata->uid = uniqid();
        $registerdata->lname = $request->input('lname');
        $registerdata->fname = $request->input('fname');
        $registerdata->mi = $request->input('mi');
        $registerdata->email = $request->input('email');
        $registerdata->phone = $request->input('phone');
        $registerdata->position = $request->input('position');
        $registerdata->bio = "Proud Member of DIGITS Organization.";
        $registerdata->profile = "default.png";
        $registerdata->save();

        $sendmessagedata = new SendMessage;
        $sendmessagedata->chatid = $registerdata->uid;
        $sendmessagedata->senderid = "admin-607add0a06445";
        $sendmessagedata->message = "Hello ".$registerdata->fname.", thank you for being a part of our organization. We hope for your active participation towards a great success for our organization. Welcome and enjoy friend, have a great day.";
        $sendmessagedata->type = "message";
        $sendmessagedata->file = "null";
        $sendmessagedata->read = "unread";
        $sendmessagedata->save();

        session()->put('toastType','success-register');
        return redirect()->route('login.index');
    }
}
