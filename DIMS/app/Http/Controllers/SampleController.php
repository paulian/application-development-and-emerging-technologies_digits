<?php

namespace App\Http\Controllers;

use App\Models\SendMessage;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = SendMessage::all();
        echo json_encode($results);
    }

    public static function sample(){
       // $b = "hello";
        $b = SendMessage::all();
        return $b;
    }
}
