<?php

namespace App\Http\Controllers;

use App\Models\Officers;
use App\Models\SendMessage;
use Illuminate\Http\Request;

class AdminOfficersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $officers = Officers::all()->reverse();
            session()->put('admin-location', 'officers');
            return view('admin.officers', compact('officers'));
        }else{
            return redirect()->route('login.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Officers $officer)
    {
        $officer->update($request->all());
        session()->put('toastType', 'officer-updated');
        return redirect()->route('officers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Officers $officer)
    {
        $uid = $officer->uid;
        //delete conversation
        $messages = SendMessage::where('chatid','=',$uid)->get();
        foreach($messages as $message){
            $deleteMessage = SendMessage::find($message->id);
            //delete all files that was stored in the root folder
            $destination = "addons/assets/root/mfiles/";
            if($deleteMessage->file != "null"){
                unlink($destination. $deleteMessage->file);
            }
            $deleteMessage->delete();
        }
        //delete profile that was stored in the root folder
        if($officer->profile != "default.png"){
            $destination = "addons/assets/root/profiles/";
            unlink($destination. $officer->profile);
        }
        //delete officer information
        $officer->delete();

        return redirect()->route('officers.index');
    }
}
