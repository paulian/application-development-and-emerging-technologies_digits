<?php

namespace App\Http\Controllers;

use App\Models\Events;
use App\Models\Announcements;
use Illuminate\Http\Request;

class AdminEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('admin-uid')){
            $events = Events::all()->reverse();
            session()->put('admin-location', 'events');
            return view('admin.events', compact('events'));
        }else{
            return redirect()->route('login.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eventdata = new Events;
        $eventdata->name = $request->input('name');
        
        if($request->theme){
            $eventdata->theme = $request->input('theme');
        }
        else{
            $eventdata->theme = "N/A";
        }

        $eventdata->venue = $request->input('venue');
        $eventdata->date = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
        $eventdata->organizer1 = $request->input('organizer1');

        /**
         * Store default data into database if input is empty
         */
        if($request->organizer2){
            $eventdata->organizer2 = $request->input('organizer2');
        }
        else{
            $eventdata->organizer2 = "N/A";
        }

        if($request->organizer3){
            $eventdata->organizer3 = $request->input('organizer3');
        }
        else{
            $eventdata->organizer3 = "N/A";
        }

        if($request->organizer4){
            $eventdata->organizer4 = $request->input('organizer4');
        }
        else{
            $eventdata->organizer4 = "N/A";
        }
        $eventdata->save();

        /**
         * This is for announcement purpose of the event
         */
        $announcementdata = new Announcements;
        $announcementdata->announcer = "Admin";
        $announcementdata->details = 'EVENT | '.'What: '.$request->name.' / '.'Where: '.$request->venue.' / '.'When: '.$eventdata->date.' / Officers you are invited to come and join the event.'; 
        $announcementdata->save();
        session()->put('toastType', 'event-added');
        return redirect()->route('events.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventdata = Events::find($id);
        $eventdata->name = $request->input('name');
        
        if($request->theme){
            $eventdata->theme = $request->input('theme');
        }
        else{
            $eventdata->theme = "N/A";
        }

        $eventdata->venue = $request->input('venue');
        $eventdata->date = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
        $eventdata->organizer1 = $request->input('organizer1');

        /**
         * Store default data into database if input is empty
         */
        if($request->organizer2){
            $eventdata->organizer2 = $request->input('organizer2');
        }
        else{
            $eventdata->organizer2 = "N/A";
        }

        if($request->organizer3){
            $eventdata->organizer3 = $request->input('organizer3');
        }
        else{
            $eventdata->organizer3 = "N/A";
        }

        if($request->organizer4){
            $eventdata->organizer4 = $request->input('organizer4');
        }
        else{
            $eventdata->organizer4 = "N/A";
        }
        $eventdata->save();

        /**
         * This is for announcement purpose of the event
         */
        $announcementdata = new Announcements;
        $announcementdata->announcer = "Admin";
        $announcementdata->details = 'EVENT UPDATE | '.'What: '.$request->name.' / '.'Where: '.$request->venue.' / '.'When: '.$eventdata->date.' / Officers you are invited to come and join the event.'; 
        $announcementdata->save();
        session()->put('toastType', 'event-updated');
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Events $event)
    {
        $event->delete();
        return redirect()->route('events.index');
    }
}
