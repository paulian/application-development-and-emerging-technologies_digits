<?php

namespace App\Http\Controllers;

use App\Models\SendMessage;
use Illuminate\Http\Request;

class SendMessageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sendmessagedata = new SendMessage;
        $sendmessagedata->chatid = session('user-uid');
        $sendmessagedata->senderid = session('user-uid');
        $sendmessagedata->message = $request->input('message');
        $sendmessagedata->type = "message";
        $sendmessagedata->file = "null";
        $sendmessagedata->read = "unread";
        $sendmessagedata->save();

        return redirect()->route('user-message.index');
    }
}
