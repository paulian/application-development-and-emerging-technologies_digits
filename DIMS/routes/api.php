<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\BorrowedBooksController;
use App\Http\Controllers\ReturnedBooksController;
use App\Http\Controllers\SampleController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*routes for categories table*/

Route::resource('category', CategoriesController::class)->only([
    'index', 'show'
]);

Route::resource('sample', SampleController::class)->only([
    'index', 'show'
]);
Route::resource('sampleinsert', SampleController::class)->only([
    'store', 'post'
]);
Route::get('samplefind', 'App\Http\Controllers\SampleController@show');

Route::resource('categoryinsert', CategoriesController::class)->only([
    'store', 'post'
]);

Route::put('categoryupdate/{id}', 'App\Http\Controllers\CategoriesController@update');

Route::delete('categorydelete/{id}', 'App\Http\Controllers\CategoriesController@destroy');

/*routes for books table*/

Route::resource('books', BooksController::class)->only([
    'index', 'show'
]);

Route::resource('booksinsert', BooksController::class)->only([
    'store', 'post'
]);

Route::put('booksupdate/{id}', 'App\Http\Controllers\BooksController@update');

Route::delete('booksdelete/{id}', 'App\Http\Controllers\BooksController@destroy');

Route::get('booksfind/{id}', 'App\Http\Controllers\BooksController@bookfind');

Route::post('validatecategory/{id}', 'App\Http\Controllers\BooksController@show');

/*routes for patrons table*/

Route::resource('patrons', PatronController::class)->only([
    'index', 'show'
]);

Route::resource('patronsinsert', PatronController::class)->only([
    'store', 'post'
]);

Route::put('patronsupdate/{id}', 'App\Http\Controllers\PatronController@update');

Route::delete('patronsdelete/{id}', 'App\Http\Controllers\PatronController@destroy');

Route::get('patronsfind/{id}', 'App\Http\Controllers\PatronController@patronfind');

/*routes for borrowed books table*/

Route::resource('borrowedbooks', BorrowedBooksController::class)->only([
    'index', 'show'
]);

Route::resource('borrowedbooksinsert', BorrowedBooksController::class)->only([
    'store', 'post'
]);

Route::put('borrowedbooksupdate/{id}', 'App\Http\Controllers\BorrowedBooksController@update');

Route::delete('borrowedbooksdelete/{id}', 'App\Http\Controllers\BorrowedBooksController@destroy');

Route::post('validateborrowbookcopies/{id}', 'App\Http\Controllers\BorrowedBooksController@show');

/*routes for returned books table*/

Route::resource('returnedbooks', ReturnedBooksController::class)->only([
    'index', 'show'
]);

Route::resource('returnedbooksinsert', ReturnedBooksController::class)->only([
    'store', 'post'
]);

Route::put('returnedbooksupdate/{id}', 'App\Http\Controllers\ReturnedBooksController@update');

Route::delete('returnedbooksdelete/{id}', 'App\Http\Controllers\ReturnedBooksController@destroy');

Route::post('validatereturnbookcopies/{id}', 'App\Http\Controllers\ReturnedBooksController@show');








