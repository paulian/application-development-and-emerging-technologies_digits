<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SendMessageController;

/** this is a sample line */
use App\Http\Controllers\SampleController;

use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AdminOfficersController;
use App\Http\Controllers\AdminMessageController;
use App\Http\Controllers\AdminEventsController;
use App\Http\Controllers\AdminMeetingsController;
use App\Http\Controllers\AdminAnnouncementsController;
use App\Http\Controllers\AdminSignoutController;
use App\Http\Controllers\AdminMessageSelectedController;
use App\Http\Controllers\AdminThemeController;
use App\Http\Controllers\AdminUploadFileController;

use App\Http\Controllers\OfficerDashboardController;
use App\Http\Controllers\OfficerOfficersController;
use App\Http\Controllers\OfficerMessageController;
use App\Http\Controllers\OfficerAccountController;
use App\Http\Controllers\OfficerSignoutController;
use App\Http\Controllers\OfficerThemeController;
use App\Http\Controllers\OfficerUploadProfileController;
use App\Http\Controllers\OfficerUploadFileController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::resource('/login', LoginController::class);
Route::resource('/register', RegisterController::class);

Route::resource('admin/dashboard', AdminDashboardController::class);
Route::resource('admin/officers', AdminOfficersController::class);
Route::resource('admin/messages', AdminMessageController::class);
Route::resource('admin/events', AdminEventsController::class);
Route::resource('admin/meetings', AdminMeetingsController::class);
Route::resource('admin/announcements', AdminAnnouncementsController::class);
Route::resource('admin/signout', AdminSignoutController::class);
Route::resource('admin/theme', AdminThemeController::class);
Route::resource('admin/open-message', AdminMessageController::class);
Route::resource('admin/message-selected', AdminMessageSelectedController::class);
Route::resource('admin/send-message', SendMessageController::class);
Route::resource('admin/upload-file', AdminUploadFileController::class);

Route::resource('officer/user-dashboard', OfficerDashboardController::class);
Route::resource('officer/user-officers', OfficerOfficersController::class);
Route::resource('officer/user-message', OfficerMessageController::class);
Route::resource('officer/user-account', OfficerAccountController::class);
Route::resource('officer/user-signout', OfficerSignoutController::class);
Route::resource('officer/send-message', SendMessageController::class);
Route::resource('officer/user-theme', OfficerThemeController::class);
Route::resource('officer/user-profile', OfficerUploadProfileController::class);
Route::resource('officer/upload-file', OfficerUploadFileController::class);

/** this is a sample line */
Route::resource('sample', SampleController::class);

Route::get('/game', function() {
    return view('game');
});

Route::get('/about', function () {
    return view('about');
});